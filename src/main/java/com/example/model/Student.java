package com.example.model;

public class Student {
	protected String name;
	protected String npm;
	protected double gpa;
	
	public Student(){
		this.name = "";
		this.npm = "";
		this.gpa = 0.0;
	}
	
	public Student(String npm,String name,double gpa){
		this.name = name;
		this.npm = npm;
		this.gpa = gpa;
	}
	
	public void setNama(String name){
		this.name = name;
	}
	
	public void setNpm(String npm){
		this.npm = npm;
	}
	
	public void setGpa(Double gpa){
		this.gpa = gpa;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getNpm(){
		return this.npm;
	}
	
	public double getGpa(){
		return this.gpa;
	}

}
