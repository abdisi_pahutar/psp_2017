package com.example.service;

import java.util.ArrayList;
import java.util.List;

import com.example.model.Student;

public class InMemoryStudentService implements StudentService {
	private static List<Student> studentList = new ArrayList<Student>();

	@Override
	public Student selectStudent(String npm) {
		Student retVal = null;
		
		for(int i = 0; i < studentList.size(); i++){
			if(studentList.get(i).getNpm().equalsIgnoreCase(npm)){
				retVal = studentList.get(i);
				break;
			}
		}
		
		return retVal;
	}
	
	@Override
	public List<Student> selectAllStudents() {
		return studentList;
    }
	
	@Override
	public void addStudent(Student student) {
		studentList.add(student);
	}

	@Override
	public void deleteStudent(String npm) {
		Student toRemove = selectStudent(npm);
		studentList.remove(toRemove);
	}
}