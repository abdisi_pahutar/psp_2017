package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PspWeek3Application {

	public static void main(String[] args) {
		SpringApplication.run(PspWeek3Application.class, args);
	}
}
